# GNS3-Juniper Appliance 

Custom GNS3 Appliances for Juniper products. These exist because the standard ones in the GNS3-Registry do not properly support these products. The format of the .gns3a file is not compatible with multiple versions of this customization.

### Usage:
#### vSRX:
- Only change is to number of interfaces (increased to 8 in order properly align)
#### vMX:
- Two appliances: RE and FPC.
	- RE:Three different versions of the RE are available, Single RE, RE0 and Re1. If you use RE0, it will expect RE1, so if running a single RE vMX just use the "single" version.
	- FPC: 
		- Removed the "single" version, just use FPC0.
		- Fixed interface count and naming
		- CPU throttling set to 25%. It will take a while for it to boot, but worth it. This will also cause numerous "Oinker" syslog messages on the console. The following configuration change will surpress those.
		`set system syslog user * match "!(.*Scheduler Oinker*.|.*Frame 0*.|.*ms without yielding*.)"`
		- If using multiple RE's simply put a GNS3 built-in switch between the RE's and the FPC(s) instead of directly connecting from RE to FPC.
		- Support for up to 4 FPC's (FPC0-3). More can be used as Juniper provides up the files required up to FPC11 for a total of 12 FPC's.
