{
    "name": "vMX FPC",
    "category": "router",
    "description": "The vMX is a full-featured, carrier-grade virtual MX Series 3D Universal Edge Router that extends 15+ years of Juniper Networks edge routing expertise to the virtual realm. This appliance is for the Virtual Forwarding Plane (vFP) VM and is meant to be paired with the Virtual Control Plane (vCP) VM.",
    "vendor_name": "Juniper",
    "vendor_url": "https://www.juniper.net/us/en/",
    "documentation_url": "http://www.juniper.net/techpubs/",
    "product_name": "Juniper vMX FPC",
    "product_url": "http://www.juniper.net/us/en/products-services/routing/mx-series/vmx/",
    "registry_version": 3,
    "status": "experimental",
    "maintainer": "none",
    "maintainer_email": "developers@gns3.net",
    "usage": "Initial username is root, password is root.\n\nCPU Throttling set at 25%, this will cause numerous \"oinker\" syslog messages. To supress these messages:\n\nset system syslog user * match \"!(.*Scheduler Oinker*.|.*Frame 0*.|.*ms without yielding*.)\"\n",
    "symbol": ":/symbols/affinity/square/gray/switch_multilayer.svg",
    "first_port_name": "Eth0",
    "port_name_format": "Eth{port1}",
    "qemu": {
        "adapter_type": "virtio-net-pci",
        "adapters": 12,
        "ram": 4096,
        "hda_disk_interface": "ide",
        "arch": "x86_64",
        "console_type": "telnet",
        "kvm": "require",
        "cpu_throttling": 10,
        "options": "-nographic -enable-kvm -smp cpus=3"
    },
    "custom_adapters": [
                {
                    "adapter_number": 0,
                    "port_name": "IGNORE"
                },
                {
                    "adapter_number": 1,
                    "port_name": "To-RE"
                },
                {
                    "adapter_number": 2,
                    "port_name": "IGNORE"
                },
                {
                    "adapter_number": 3,
                    "port_name": "ge-0/0/0"
                },
                {
                    "adapter_number": 4,
                    "port_name": "ge-0/0/1"
                },
                {
                    "adapter_number": 5,
                    "port_name": "ge-0/0/2"
                },
                {
                    "adapter_number": 6,
                    "port_name": "ge-0/0/3"
                },
                {
                    "adapter_number": 7,
                    "port_name": "ge-0/0/4"
                },
                {
                    "adapter_number": 8,
                    "port_name": "ge-0/0/5"
                },
                {
                    "adapter_number": 9,
                    "port_name": "ge-0/0/6"
                },
                {
                    "adapter_number": 10,
                    "port_name": "ge-0/0/7"
                },
                {
                    "adapter_number": 11,
                    "port_name": "ge-0/0/8"
                },
                {
                    "adapter_number": 12,
                    "port_name": "ge-0/0/9"
                }
            ],
    "images": [
        {
            "filename": "vFPC-20180605.img",
            "version": "FPC0 18.2R1.9",
            "md5sum": "9b03ee3355174d100fb72054755f98de",
            "filesize": 2313158656,
            "download_url": "http://www.juniper.net/us/en/products-services/routing/mx-series/vmx/"
        },
        {
            "filename": "metadata-usb-fpc0.img",
            "version": "FPC0 18.2R1.9",
            "md5sum": "3d2a7f88fd974864d8d7ada068f95342",
            "filesize": 16777216,
            "download_url": "http://www.juniper.net/us/en/products-services/routing/mx-series/vmx/"
        },
        {
            "filename": "vFPC-20180605.img",
            "version": "FPC1 18.2R1.9",
            "md5sum": "9b03ee3355174d100fb72054755f98de",
            "filesize": 2313158656,
            "download_url": "http://www.juniper.net/us/en/products-services/routing/mx-series/vmx/"
        },
        {
            "filename": "metadata-usb-fpc1.img",
            "version": "FPC1 18.2R1.9",
            "md5sum": "ef0b8fc70193e3e561687f764ff4d84d",
            "filesize": 16777216,
            "download_url": "http://www.juniper.net/us/en/products-services/routing/mx-series/vmx/"
        },
        {
            "filename": "vFPC-20180605.img",
            "version": "FPC2 18.2R1.9",
            "md5sum": "9b03ee3355174d100fb72054755f98de",
            "filesize": 2313158656,
            "download_url": "http://www.juniper.net/us/en/products-services/routing/mx-series/vmx/"
        },
        {
            "filename": "metadata-usb-fpc2.img",
            "version": "FPC2 18.2R1.9",
            "md5sum": "9e42c85badb4f0a5370554bedfcc55a2",
            "filesize": 16777216,
            "download_url": "http://www.juniper.net/us/en/products-services/routing/mx-series/vmx/"
        },
        {
            "filename": "vFPC-20180605.img",
            "version": "FPC3 18.2R1.9",
            "md5sum": "9b03ee3355174d100fb72054755f98de",
            "filesize": 2313158656,
            "download_url": "http://www.juniper.net/us/en/products-services/routing/mx-series/vmx/"
        },
        {
            "filename": "metadata-usb-fpc3.img",
            "version": "FPC3 18.2R1.9",
            "md5sum": "38122541fdeb10a7c41bc92e13cb15ad",
            "filesize": 16777216,
            "download_url": "http://www.juniper.net/us/en/products-services/routing/mx-series/vmx/"
        }
    ],
    "versions": [
        {
            "name": "FPC0 18.2R1.9",
            "images": {
                "hda_disk_image": "vFPC-20180605.img",
                "hdb_disk_image": "metadata-usb-fpc0.img"
            }
        },
        {
            "name": "FPC1 18.2R1.9",
            "images": {
                "hda_disk_image": "vFPC-20180605.img",
                "hdb_disk_image": "metadata-usb-fpc1.img"
            }
        },
        {
            "name": "FPC2 18.2R1.9",
            "images": {
                "hda_disk_image": "vFPC-20180605.img",
                "hdb_disk_image": "metadata-usb-fpc2.img"
            }
        },
        {
            "name": "FPC3 18.2R1.9",
            "images": {
                "hda_disk_image": "vFPC-20180605.img",
                "hdb_disk_image": "metadata-usb-fpc3.img"
            }
        }
    ]
}
